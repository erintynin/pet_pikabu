/*
 * JsBerry
 * Source: https://github.com/Dugnist/jsberry
 *
 * default router options
 *
 * Author: Serhey Rudenko (@sergeyrudenko)
 * Released under the MIT license
 */

const modules = [];
const errorsStore = {};

const Errors = {
  /**
   * Get list of errors
   * @return {Object} errorsStore - erros store
   */
  get: function() {
    return { errors: errorsStore, modules };
  },

  /**
   * Set errors to Errors store
   * @param  {String} name - module name
   * @param  {Object} errors - avalaible errors
   * @return  {this} - for chaining
   */
  set: function(name = 'unknown', errors = {}) {
    if (modules.indexOf(name) !== -1) throw new Error('already connected modl');

    for (const key in errors) {
      if (errorsStore[key]) throw new Error(`Error with code ${key} - exist`);
      const { 
        status = 500, hint = 'connect with developer', message = 'unknown err',
        code = 500,
      } = errors[key];
      errorsStore[key] = { code, status, hint, message };
    }
    return this;
  },
  /**
   * Set errors to Errors store
   * @param  {Object} error - avalaible errors
   * @return  {Object} created error 
   */
  create: function(error = {}) {
    const { message = '', stack = '', hint } = error;
    const errFromStore = errorsStore[message];

    if (!errFromStore) { // unknown err
      const result = { 
        hint: hint || 'non-typical error, connect with developer', 
        stack, 
        message, 
        code: 500,
      };
      return { status: 500, result };
    } else { // known err
      const { code, status } = errFromStore;
      return { 
        status, 
        result: {
        message: errFromStore.message, 
        hint: hint || errFromStore.hint, 
        code: Number(code),
      } };
    }
  },

};

module.exports = Errors;
