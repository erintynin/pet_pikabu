/**
 * The root path of plugins.
 * Export your plugins here to connect it to main core.
 */

// const Websockets = require(`./websockets`);
const Passport = require('./passport');
const Validate = require('./joi_validate');
// const Redis = require('./redis');
// const Sentry = require('./sentry');
const Sequelize = require('./sequelize');
// const Sendmail = require('./sendmail');
// const AWS = require('./aws-sdk');
// const Multer = require('./multer');
// const Fetch = require('./fetch');
// const Postgresdump = require('./postgresdump');

const PLUGINS = [
  Sequelize, Passport, Validate,
];

module.exports = (CONFIG) => PLUGINS.concat(
  require(`./express-api/index`)
  // require(`./${CONFIG.framework}_api/index`)
);
