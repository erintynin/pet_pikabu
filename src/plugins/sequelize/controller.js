module.exports = ({ Sequelize/* ACTIONS, CONFIG */}) => ({
  createSchema: function(schema) {
    const result = {};
    for (const key in schema) {
      result[key] = this.createOne(schema[key]);
    }
    return result;
  },
  createOne: function(object) {
    const result = {};
    for (const key in this.convertSchema) {
      if (key in object) result[this.convertSchema[key]] = object[key];
    }
    this.addType(result, object);
    return result;
  },
  addStatics: function(statics, Model) {
    const obj = statics({methods: {}});
    for (const key in obj.methods) {
      Model.prototype[key] = obj.methods[key];
    }
    // console.log(obj);
  },
  convertSchema: {
    default: 'defaultValue',
    primaryKey: 'primaryKey',
    validate: 'validate',
    allowNull: 'allowNull',
    autoIncrement: 'autoIncrement',
    field: 'field',
  },
  addType: function(schema, object) {
    if (object.type == 'array') {
      schema.type = 
      Sequelize[object.type.
        toUpperCase()](Sequelize[object.arrayElements.toUpperCase()]);
    } else {
      schema.type = Sequelize[object.type.toUpperCase()];
    }
  },
});
