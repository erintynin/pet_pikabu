const Sequelize = require('sequelize');
const moment = require('moment');
const { credentials } = require('./config.json');
let { nameDB, nameUser, password, options } = credentials.locale ? 
credentials.locale : false;
const type = require('pg').types;


type.setTypeParser(1184, (stringValue) => {
  return moment(stringValue).format('MM/DD/YYYY, h:mm:ss A');
});
let sequelize = {};
credentials.locale ? 
sequelize = new Sequelize(`${nameDB}`, `${nameUser}`, `${password}`, options): 
sequelize = new Sequelize(credentials.databaseUri);
const OP = Sequelize.Op;
const FN = Sequelize.fn;
// const query = sequelize.query;
const controller = require('./controller');


module.exports = ({ ACTIONS, show }) => {
  const pluginController = controller({ ACTIONS, Sequelize });
  /**
   * Connect to database
   * @type {Object}
   */
  sequelize.authenticate().then(() => {
    show.log('Connected to PostgreSQL database');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });
  /**
   *****************************************
   * SUBSCRIBE TO CREATE MODEL FROM SCHEMA *
   *****************************************
   *
   * @param  {object} model - entity model
   * @param  {object} schema - entity schema
   * @param  {function} attachMethods - model methods
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.model.create', 
    async({ name, schema={}, statics, options = {} }) => {
      try {
        if (!name) return Promise.reject('Empty mongoose schema name!');

        const transformedSchema = pluginController.createSchema(schema);
        
        const Model = sequelize.define(name, transformedSchema, options);

        pluginController.addStatics(statics, Model);

        // await Model.sync({ forse: true });
        // await Model.sync(); // FIXME: rework here
        
        return Promise.resolve(Model);
      } catch (error) {
        return Promise.reject(error);
      }
  });

  ACTIONS.on('database.sync.forse', () => {
    sequelize.sync({ forse: true });
  });

  /**
   *******************************
   * SUBSCRIBE TO CHECK DATABASE *
   *******************************
   */
  ACTIONS.on('database.check', () => Promise.resolve(true));

  /**
   ****************************************
   * SUBSCRIBE TO COUNT DATABASE ENTITIES *
   ****************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.count', ({ model, payload = {} }) => {
    return model.count.bind(model)(payload);
  });

  /**
   ***************************************
   * SUBSCRIBE TO CREATE DATABASE ENTITY *
   ***************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.create', ({ model, payload = {} }) =>
    model.create.bind(model)(payload)
  );

  /**
   *************************************
   * SUBSCRIBE TO READ DATABASE ENTITY *
   *************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.read', ({ model, attributes = null, payload = {} }) =>
      model.findOne.bind(model)({ where: payload, attributes })
  );

  // /**
  //  *************************************
  //  * SUBSCRIBE TO READ DATABASE ENTITY *
  //  *************************************
  //  *
  //  * @param  {object} model - entity model
  //  * @param  {array} payload - entity data
  //  * @return {promise} - success response or error
  //  */
  // ACTIONS.on('database.fn.read', ({ model, payload = [] }) => {
  //     const { name, argument, AS } = payload.fn;
  //     return model.findOne.bind(model)({ atributes: [sequelize.fn(`${name}`, 
  //     `${argument}`, `${AS}`)] });
  // });

  /**
   * 
   * @param {*} param0
   * @return {*}  
   */
  function opConfig({ payload, op }) {
    if (op) {
      let i = 0;
      for (let key in payload) {
        if (op[i] == 'or') {
          payload[key] = { [OP[op[i]]]: payload[key]};
            i++;
        } else {
          if (Array.isArray(payload[key])) {
            let variable = { [key]: {}};
            for (let j = 0; j < payload[key].length; j++) {
              variable[key][OP[op[i]]] = payload[key][j];
              i++;
            }
            payload[key] = variable[key]; 
          } else {
            payload[key] = { [OP[op[i]]]: payload[key]};
            i++;
          }
        }
      }
    }
    return payload;
  }
  /**
   *******************************************
   * SUBSCRIBE TO READ ALL DATABASE ENTITIES *
   *******************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.readAll', ({ model, payload = {},
    order = null, limit = null, attributes = null, op = null, include}) => {
    payload = opConfig({ payload, op });
    return model.findAll.bind(model)({ 
      attributes, where: payload, order, limit, raw: true, include });
  }
  );
  /**
   ***************************************
   * SUBSCRIBE TO UPDATE DATABASE ENTITY *
   ***************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.update', async({ model, data = {}, payload = {} }) => {
    for (key in data) {
      if (data[key] === undefined) delete data[key];
    }
    const Model = await ACTIONS.send('database.read', { model, payload });
    return (Model) ? Model.update(data) : Promise.reject('404');
  });

  /**
   ***************************************
   * SUBSCRIBE TO DELETE DATABASE ENTITY *
   ***************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.delete.one', async({ model, payload = {} }) => {
    const Model = await ACTIONS.send('database.read', { model, payload });

    return (Model) ? Model.destroy() : Promise.reject('404');
  });
  

  // ACTIONS.on('database.delete.one', async({ payload = {} }) => {
  //   return Promise.resolve(query(`${payload}`));
  // });

  /**
   *****************************************
   * SUBSCRIBE TO DELETE DATABASE ENTITIES *
   *****************************************
   *
   * @param  {object} model - entity model
   * @param  {object} payload - entity data
   * @return {promise} - success response or error
   */
  ACTIONS.on('database.delete.many', ({ model, payload = {}, op = null }) => {
    payload = opConfig({ payload, op });
    return model.destroy.bind(model)({ where: payload }); // Example: { id: 1 }
  });
};
