const availableType = ['object', 'array', 'string', 
  'boolean', 'number', 'date', 'insensitive'];
/*
 * JsBerry example: validation middleware
 */

/**
 ******************************************
 * Express/connect/restify implementation *
 ******************************************
 * @param  {Events} ACTIONS
 * @param  {Events} ROUTER
 * @param  {Events} ERROR
 * @return {function} - next
 */
module.exports = (ACTIONS, ROUTER, ERROR) => 
  async(req, res = {}, next, socket = false) => {
  try {
    const allRoutes = ROUTER.get('routes');
    const routePath = socket ? req.path : req.route.path;
    // const routeMethod = Object.keys(req.route.methods)[0];

    // Find route with searched validation
    const currentRoute = socket ? req.event:
        Object.values(allRoutes).find((r) => r.path === routePath.substring(5));
    const validationSchema = socket ?
      await ACTIONS.send(`${req.valid}.validation.schema,get`):
        await ACTIONS
          .send(`${currentRoute.path.split('/')[0]}.validation.schema.get`);

    const required = currentRoute.validation.required;
    const optional = currentRoute.validation.optional;
    let parameters = {};
    // Combine (query | body | params) parameters by config values from request
    if (!socket) {
      parameters = (currentRoute.validation.parameters || []).map((type) => 
        req[type]).reduce((prev, next) => ({ ...prev, ...next }));
    } else {
      parameters = [req.body].reduce((prev, next) => ({ ...prev, ...next }));
    }
    const resultSchema = function(checkFor) {
      // TODO: start here
      const createKeyForSchema = 
        function(validationSchema, required, optional) {
          const validationOptions = {};
          const mainSchema = {};
          required.forEach((element) => {
            mainSchema[element] = checkFor;
            validationOptions[element] = { 
              required: true, ...validationSchema[element],
            };
          });
          optional.forEach((element) => {
            mainSchema[element] = checkFor;
            validationOptions[element] = { ...validationSchema[element] };
          });
          return { validationOptions, mainSchema };
      };

      const mainTransform = function(options, schema) {
        for (const key in schema) {
          schema[key] = innerTransform(schema[key], options[key]);
        }
        return schema;
      };
      
      const innerTransform = function(joi, options) {
        let { newJoi, newSchema } = addType(joi, options);
        for (const key in newSchema) {
          if (newSchema[key] === true) {
            newJoi = newJoi[key]();
          } else {
            const argument = createArgument(newSchema[key]);
            newJoi = newJoi[key](argument);
          }
        }
        return newJoi;
      };

      const createArgument = function(argument) {
        if (argument === null) throw new Error('wrong schema definition');
        if (typeof argument === 'object' && !(argument instanceof RegExp) 
          && !(Array.isArray(argument))) {
          const joi = checkFor;
          return innerTransform(joi, argument);
          // let schema = createInnerSchema(argument);
          // // const innerSchema = innerTransform(null, argument);
          // schema = mainTransform(argument, schema);
          return schema;
        }
        return argument;
      };

      // const createInnerSchema = function(schema) {
      //   const result = {};
      //   for (const key in schema) {
      //     result[key] = checkFor;
      //   }
      //   return result;
      // };

      const addType = function(joi, options) {
        for (let i = 0; i < availableType.length; i++) {
          if (options[availableType[i]] === true) {
            const newJoi = joi[availableType[i]]();
            const newSchema = Object.assign({}, options);
            delete newSchema[availableType[i]];
            return { newJoi, newSchema};
          }
        }
        throw new Error('unavailable type');
      };

      if (!required && !optional ) return false;

      let { validationOptions, mainSchema } = 
        createKeyForSchema(validationSchema, required, optional);
      
      mainSchema = mainTransform(validationOptions, mainSchema);

      return mainSchema;
    };

    // Get validation schema by name from config
    const result = await ACTIONS.send('validate.schema', {
      schema: resultSchema, payload: parameters,
    });

    if (result.error && result.error.message) {
      throw new Error(result.error.message);
    }

    return socket ? { success: true }: next();
  } catch (error) {
    error.hint = error.message;
    error.message = 400;
    if (!socket) {
      const { status, result } = ERROR.create(error);
      res.status(status).send(result);
    } else {
      return ERROR.create(error);
    }
  }
};
