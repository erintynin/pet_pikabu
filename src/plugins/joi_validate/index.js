const Joi = require('joi');
const { errors } = require('./config.json');

const validationMiddleware = require('./validation.middleware');

module.exports = ({ ACTIONS, ROUTER, ERROR }) => {
  ERROR.set('joi', errors);
  /**
   * Validation middleware
   */
  ROUTER.set('middlewares', {
    validationMiddleware: validationMiddleware(ACTIONS, ROUTER, ERROR),
  }, 'routes');

  ACTIONS.on('validate.schema', ({ schema = {}, payload = {} }) => {
    const joiSchema = Joi.object().keys(schema(Joi));

    return Promise.resolve(Joi.validate(payload, joiSchema));
  });
};
