/*
 * identity middleware
 */

 /**
  ******************************************
  * Express/connect/restify implementation *
  ******************************************
  * @param  {Events} ACTIONS
  * @param  {Events} ERROR
  * @param  {function} req - request
  * @param  {function} res - response
  * @param  {function} next - next
  * @return {function} - next
  */
 module.exports = (ACTIONS, ERROR) => async(req, res, next) => {
  try {
    // const passport = await ACTIONS.send('passport.return');
    const idToken = req.query.access_token;

    getInfoByIdToken(idToken).then(
    ({ err, user, profile }) => {
      if (err || (profile && profile.stack)) {
        const { status, result } = ERROR.create(new Error('406'));
        return res.status(status).send(result);
      }
      if (user) (req.auth = user, next());
      if (profile) (req.profile = profile, next());
    });
    // (req, res, next);
  } catch (error) {
    const { status, result } = ERROR.create(error);
    res.status(status).send(result);
  }

/**
 * 
 * @param {*} idToken - auth id token
 * @return {*} promise - err, user, profile
 */
async function getInfoByIdToken(idToken) {
  try {
    // { name, picture, email, sub? }
    const profile = await ACTIONS.send('fetch.get', {
      url: `https://oauth2.googleapis.com/tokeninfo?id_token=${idToken}`,
    });
    if (profile.stack) throw new Error();
    const email = profile.email || '';
    let user = await ACTIONS.send('users.custom.query', {
      type: 'read', payload: { google: profile.sub },
    });
    user ? true : user = await ACTIONS.send('users.custom.query', {
      type: 'read', payload: { email },
    });

    const ggprofile = {
      email,
      google: profile.sub,
      photo: profile.picture,
      nickname: profile.given_name,
    };

    if (user) {
      if (user.google === '') { 
        await user.updateGoogle(profile.sub, profile.picture);
      }
      return Promise.resolve({ err: null, user: user, profile: null });
    } else {
      return Promise.resolve( { err: null, suser: null, profile: ggprofile });
    }
  } catch (error) {
    return Promise.resolve({ err: error, user: null, profile: null });
  }
};
};
