/*
 * identity middleware
 */

 /**
  ******************************************
  * Express/connect/restify implementation *
  ******************************************
  * @param  {Events} ACTIONS
  * @param  {Events} ERROR
  * @param  {function} req - request
  * @param  {function} res - response
  * @param  {function} next - next
  * @return {function} - next
  */
 module.exports = (ACTIONS, ERROR) => async(req, res, next) => {
    try {
      const passport = await ACTIONS.send('passport.return');

      passport.authenticate('google', 
      (err, user, profile = { emails: [{value: ''}] }) => {
        if (err) {
          const { status, result } = ERROR.create({ message: err });
          res.status(status).send(result);
        } 
        req.auth = user; 
        req.body = profile._json;
        (user) ? (req.auth = user, next()) : next();
      })(req, res, next);
    } catch (error) {
      const { status, result } = ERROR.create(error);
      res.status(status).send(result);
    }
  };
