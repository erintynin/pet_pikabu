/*
 * identity middleware
 */

 /**
  ******************************************
  * Express/connect/restify implementation *
  ******************************************
  * @param  {Events} ACTIONS
  * @param  {Events} ERROR
  * @param  {function} req - request
  * @param  {function} res - response
  * @param  {function} next - next
  * @return {function} - next
  */
 module.exports = (ACTIONS, ERROR) => async(req, res, next) => {
  try {
    const passport = await ACTIONS.send('passport.return');
    passport.authenticate('facebook-token', req.query.access_token, 
      (err, user, profile) => {
          console.log(err, user, profile);
        if (err || (profile && profile.stack)) {
          const { status, result } = ERROR.create(new Error('406'));
          return res.status(status).send(result);
        } 
        if (user) (req.auth = user, next());
        if (profile) (req.profile = profile, next());
    })(req, res, next); 
  } catch (error) {
    res.send({ error: 401, message: error.message });
  }
};
