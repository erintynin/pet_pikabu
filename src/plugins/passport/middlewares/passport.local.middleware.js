/*
 * JsBerry example: passport local authorization middleware
 *
 * warning: required database plugin
 */

/**
 ******************************************
 * Express/connect/restify implementation *
 ******************************************
 * @param  {Events} ACTIONS
 * @param  {Events} ERROR
 * @return {function} - next
 */
module.exports = (ACTIONS, ERROR) => async(req, res, next) => {
    try {
        const passport = await ACTIONS.send('passport.return');
        passport.authenticate('local', (err, user) => {
            if (err) {
                const { status, result } = ERROR.create(err);
                return res.status(status).send(result);
            } 
            (user) ? (req.auth = user, next()) : next();
        })(req, res, next);
    } catch (error) {
        const { status, result } = ERROR.create(error);
        res.status(status).send(result);
    }
};
