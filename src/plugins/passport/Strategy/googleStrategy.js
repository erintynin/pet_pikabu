const GoogleStrategy = require('passport-google-oauth2').Strategy;

module.exports = ({ options, passport, ACTIONS }) => {
  passport.use(new GoogleStrategy(options,
    async function(
      token, refreshToken, profile = { emails: [{value: ''}]}, done) {
      try {
        const data = {
          type: 'read', payload: { email: (profile.emails[0] || {}).value },
        };
        
        const user = await ACTIONS.send('users.custom.query', data);

        if (user) {
            done(null, user, profile);
        } else return done(null, false, profile);
      } catch (error) {
        return done(error, profile);
      }
    }
  ));
};
