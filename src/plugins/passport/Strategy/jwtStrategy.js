const JwtStrategy = require('passport-jwt').Strategy; // Auth via JWT

module.exports = ({ options, passport, ACTIONS }) => {
    passport.use(new JwtStrategy(options, async(payload, done) => {
        try {
        if (!payload) throw new Error('401');
        if (Date.now() > payload.expire) throw new Error('407');

        const data = { type: 'read', payload: { id: payload.id }};
        const user = await ACTIONS.send('users.custom.query', data);
        
        return (user) ? done(null, user) : done(null, false);
        } catch (error) {
        return done(error, false);
        }
  }));
};
