const LocalStrategy = require('passport-local').Strategy;
const crypto = require('crypto');

module.exports = ({ options, passport }) => {
  passport.use(new LocalStrategy( options,
    async(email, password, done) => {
    try {
      const data = { type: 'read', payload: { email } };
      const user = await ACTIONS.send('users.custom.query', data);
      const hash = crypto.createHmac('sha256', CONFIG.secretkey)
        .update(password)
        .digest('hex');

      if (user) {
        if (!(hash === user.password)) throw new Error('405');
        done(null, user);
      } else return done(null, false);
    } catch (error) {
      done(error, null);
    }
  }));
};
