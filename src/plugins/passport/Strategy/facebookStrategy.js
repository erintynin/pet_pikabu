const FacebookStrategy = require('passport-facebook').Strategy;

module.exports = ({ options, passport, ACTIONS }) => {
    passport.use(new FacebookStrategy( options,
        async function(accessToken, refreshToken, profile = {}, done) {
      try {
        const data = {
          type: 'read', payload: { email: (profile._json || {}).email },
        };
        
        const user = await ACTIONS.send('users.custom.query', data);

        if (user) {
            done(null, user, profile);
        } else return done(null, false, profile);
      } catch (error) {
        return done(error, profile);
      }
    }
  ));
};
