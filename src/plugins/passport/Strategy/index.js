const facebookStrategy = require('./facebookStrategy');
const googleStrategy = require('./googleStrategy');
const localStrategy = require('./localStrategy');
const jwtStrategy = require('./jwtStrategy');

module.exports = { 
    facebookStrategy, googleStrategy, localStrategy, jwtStrategy,
};
