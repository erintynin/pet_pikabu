const FacebookToken = require('passport-facebook-token');

module.exports = ({ options, passport, ACTIONS }) => {
  passport.use(new FacebookToken(options,
    async function(accessToken, refreshToken, profile, done) {
    try {
      const email = profile._json.email || '';
      let user = await ACTIONS.send('users.custom.query', {
        type: 'read', payload: { facebook: profile._json.id },
      });
      user ? true : user = await ACTIONS.send('users.custom.query', {
        type: 'read', payload: { email },
      });

      const fsprofile = { 
        email,
        facebook: profile._json.id,
        photo: profile.photos[0].value,
        nickname: profile._json.first_name,
      };

      if (user) {
        if (user.facebook === '') { 
          await user.updateFacebook(profile._json.id, profile.photos[0].value);
        }
        done(null, user, null);
      } else {
        done(null, null, fsprofile);
      }
    } catch (error) {
      done(error, null, null);
    }
  }));
};

