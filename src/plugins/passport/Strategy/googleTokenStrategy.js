const GoogleTokenStrategy = require('passport-google-token').Strategy;

module.exports = ({ options, passport, ACTIONS }) => {
  passport.use(new GoogleTokenStrategy(options,
    async function(accessToken, refreshToken, profile, done) {
    try {
      const email = profile._json.email || '';
      let user = await ACTIONS.send('users.custom.query', {
        type: 'read', payload: { google: profile._json.id },
      });
      user ? true : user = await ACTIONS.send('users.custom.query', {
        type: 'read', payload: { email },
      });

      const ggprofile = {
        email,
        google: profile._json.id,
        photo: profile._json.picture,
        nickname: profile._json.given_name,
      };

      if (user) {
        if (user.google === '') { 
          await user.updateGoogle(profile._json.id, profile._json.picture);
        }
        done(null, user, null);
      } else {
        done(null, null, ggprofile);
      }
    } catch (error) {
      done(error, null, null);
    }
  }));
};
