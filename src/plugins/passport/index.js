const passport = require('passport');
const strategy = require('./Strategy/index');
const listStrategy = (process.env.DOCKER) ?
  require('./config.prod.json').strategy: require('./config.dev.json').strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt; // Auth via JWT
const jwt = require('jsonwebtoken'); // auth via JWT for http

const facebookCbMiddleware =
  require('./middlewares/passport.facebook.cb.middleware');
const googleCbMiddleware =
  require('./middlewares/passport.google.cb.middleware');
const passportLocalMiddleware =
  require('./middlewares/passport.local.middleware');
const passportJWTMiddleware =
  require('./middlewares/passport.jwt.middleware');

module.exports = async({ ACTIONS, ROUTER, CONFIG, ERROR }) => {
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
    secretOrKey: CONFIG.secretkey,
  };

  if (listStrategy.jwtStrategy) listStrategy.jwtStrategy.options = jwtOptions;
  
  for (key in listStrategy) {
    strategy[key]({ options: listStrategy[key].options, passport, ACTIONS });
  }
  /**
   ********************************************
   * ADD PASSPORT MIDDLEWARE TO ACTIONS TREAD *
   ********************************************
   */
  ROUTER.set('middlewares', { 
    passportMiddleware: passport.initialize(),
    googleCbMiddleware: googleCbMiddleware(ACTIONS, ERROR),
    facebookCbMiddleware: facebookCbMiddleware(ACTIONS, ERROR),
    passportLocalMiddleware: passportLocalMiddleware(ACTIONS, ERROR),
    passportJWTMiddleware: passportJWTMiddleware(ACTIONS, ERROR),
  }, 'routes');

  /**
   * Create jwt token
   */
  ACTIONS.on('passport.create.jwt', (payload = {}) =>
    Promise.resolve(jwt.sign(payload, CONFIG.secretkey)));

  /**
   * Return passport instance
   */
  ACTIONS.on('passport.return', () => Promise.resolve(passport));
};
