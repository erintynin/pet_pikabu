/* eslint-disable max-len */
module.exports = {
   body: {
    string: true,
    max: 1000,
    error: () => `Invalid body, max length 1000.`,
   },
};
