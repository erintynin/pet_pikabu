// const comentSchema = require('./coments/coment.schema');
const postSchema = require('./posts/post.schema');
const postRatingsSchema = require('./postRatings/postRatings.schema');
// const subscriberSchema = require('./subscribers/subscriber.schema');
// const subscriptionSchema = require('./subscriptions/subscription.schema');
// const userSchema = require('./users/user.schema');


// const comentStatics = require('./coments/coment.statics');
const postStatics = require('./posts/post.statics');
const postRatingsStatics = require('./postRatings/postRatings.statics');
// const subscriberStatics = require('./subscribers/subscriber.statics');
// const subscriptionStatics = require('./subscriptions/subscription.statics');
// const userStatics = require('./users/user.statics');

// const comentValidation = require('./coments/coment.validation');
const postValidation = require('./posts/post.validation');
const postRatingsValidation = require('./postRatings/postRatings.validation');
// const subscriberValidation = require('./subscribers/subscriber.validation');
// const subscriptionValid = require('./subscriptions/subscription.validation');
// const userValidation = require('./users/user.validation');

// const saveOptions = require('./saves/save.options');

module.exports = {
    postRatingsEntity: {
        schema: postRatingsSchema,
        statics: postRatingsStatics,
        validation: postRatingsValidation,
        relationships: () => {},
    },
    // comentEntity: {
    //     schema: comentSchema,
    //     statics: comentStatics,
    //     validation: comentValidation,
    //     relationships: () => {},
    // },
    postEntity: {
        schema: postSchema,
        statics: postStatics,
        validation: postValidation,
        relationships: () => {},
    },
    // saveEntity: {
    //     schema: saveSchema,
    //     statics: saveStatics,
    //     validation: saveValidation,
    //     options: saveOptions,
    //     relationships: () => {},
    // },
    // subscriberEntity: {
    //     schema: subscriberSchema,
    //     statics: subscriberStatics,
    //     validation: subscriberValidation,
    //     relationships: () => {},
    // },
    // subscriptionEntity: {
    //     schema: subscriptionSchema,
    //     statics: subscriptionStatics,
    //     validation: subscriptionValid,
    //     relationships: () => {},
    // },
};
