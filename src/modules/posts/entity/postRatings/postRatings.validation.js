/* eslint-disable max-len */
module.exports = {
   body: {
    string: true,
    regex: new RegExp(/^[а-яА-ЯёЁa-zA-Z0-9]{3,30}/g),
    error: () => `Invalid body, max length 1000.`,
   },
};
