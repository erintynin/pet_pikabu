const { routes } = require('./config.json');
// Import middlewares
const {

} = require('./middlewares/index');
// Import controllers
const { _postController } = require('./controllers/index');
// Import services
const {
    _postService, _fsService,
} = require('./services/index');
// Import entity
const { 
    postEntity,
    postRatingsEntity,
    
} = require('./entity/index');

const postSubscriptions = require('./subscriptions');

/**
 * Configurate user module
*/
module.exports = async({ ACTIONS, ROUTER, ERROR, CONFIG, Model, utils }) => {
    /**
    * Add config routes to global ROUTER
    */
    ROUTER.set('routes', routes);
    /**
    * Create user MODEL from schema
    */
    const postModel = new Model('posts', postEntity);
    const postRatingsModel = new Model('postratings', postRatingsEntity);
    const models = Model.getAllModelList();
    /**
    * Pin ACTIONS to services
    */
    const postService = _postService({ ACTIONS, postModel, models });
    const fsService = _fsService({ ACTIONS });
    /**
    * Pin ACTIONS to controllers
    */
    const postController = _postController({
        ACTIONS, CONFIG, postService, fsService,
    });
    /**
    * Transform route names to ACTIONS commands
    */
    const postCommands = utils.convertKeysToDots(routes);
    /**
    * Transform route names to ACTIONS commands
    */
    postSubscriptions({
        ACTIONS, postController, postCommands, postEntity, postService, Model,
    });
    /**
    * Add user middlewares to global ROUTER
    */   
    // ROUTER.set('middlewares', {
    //     postExistCheckMiddleware: postExistCheckMiddleware({
    //         ACTIONS, ERROR, postService,
    //     }),
    //     postPermissionMiddleware: postPermissionMiddleware({
    //         ACTIONS, ROUTER, CONFIG, ERROR,
    //     }),
    // }, 'routes');
};
