module.exports = ({
    ACTIONS, postController, postCommands, postEntity, postService, Model,
}) => {
    /**
     * Extract user commands
     */
    
    const {
        // eslint-disable-next-line camelcase
        posts_create, posts_hot, posts_best, posts_new,
        // eslint-disable-next-line camelcase
        users_upemail, users_uppassword, users_get,
        // 
        // users_delete,
        // users_all,
    } = postCommands;
    /**
     * Extract user controller methods
     */
    const {
        createPost,
        getHot,
        getBest,
        getNew,
        // parseProfile,
        // comparePassword,
        // getUser,
        // updateUser,
        // passwordHash,
        // deleteUser,
        // getAllUsers,
    } = postController;    

    /**
     * Subscribe to posts create
     */
    ACTIONS.on(posts_create, async({ body, auth }) => {
        try {
            // checkers
            const models = Model.getAllModelList();
            if (!auth) throw new Error('User already exist!');
            return auth.createPost({ body: body.body, postrating: {}},
            {
                include: models['postratings'],
            });
        } catch (error) {
            const message = error.message || error;
            return Promise.reject({ code: 400, message, ...error });
        }
    });

    /**
     * Subscribe to get hot posts
     */
    ACTIONS.on(posts_hot, async({ auth }) => {
        try {
            // checkers
            if (!auth) throw new Error('No such user here!');
            // controller call's
            const result = await getHot();
            
            return result;
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    /**
     * Subscribe to get best posts
     */
    ACTIONS.on(posts_best, async({ auth }) => {
        try {
            // checkers
            if (!auth) throw new Error('No such user here!');
            // controller call's
            const result = await getBest();
            
            return result;
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    ACTIONS.on(posts_new, async({ auth }) => {
        try {
            // checkers
            if (!auth) throw new Error('No such user here!');
            // controller call's
            const result = await getNew();
            
            return result;
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    // /**
    //  * Subscribe to user auth facebook 
    //  */
    // ACTIONS.on(users_facebook, async({ auth, body }) => {
    //     try {
    //         // checkers
    //         if (!auth && !body) throw new Error('No such user here!');
    //         if (!auth) auth = await createUser(await parseProfile(body));
    //         // controller call's
    //         const result = await getSelf({ auth });

    //         return result.publicKeys();
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // /**
    //  * Subscribe to user auth facebook 
    //  */
    // ACTIONS.on(users_google, async({ auth, body }) => {
    //     try {
    //         // checkers
    //         if (!auth && !body) throw new Error('No such user here!');
    //         if (!auth) auth = await createUser(await parseProfile(body));
    //         // controller call's
    //         const result = await getSelf({ auth });

    //         return result.publicKeys();
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // // /**
    // //  * Subscribe to get one user
    // //  */
    // ACTIONS.on(users_get, async({ params }) => {
    //     try {
    //         // controller call's
    //         const result = await getUser({ id: params.id });

    //         return result.publicKeys();
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // /**
    //  * Subscribe to update user
    //  */
    // ACTIONS.on(users_update, async({ body, auth }) => {
    //     try {
    //         if (!auth) throw new Error('No such user here!');

    //         const result = await updateUser({ body, auth });

    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // /**
    //  * Subscribe to update email user
    //  */
    // ACTIONS.on(users_upemail, async({ body, auth }) => {
    //     try {
    //         if (!auth) throw new Error('No such user here!');
            
    //         const compare = await comparePassword({ auth, body });
    //         if (!compare) throw new Error(compare.error);

    //         return await updateUser({ body: { email: body.email }, auth });
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // ACTIONS.on(users_uppassword, async({ body, auth }) => {
    //     try {
    //         if (!auth) throw new Error('No such user here!');
            
    //         const compare = await comparePassword({ auth, body });
    //         if (!compare) throw new Error(compare.error);
    //         const hash = await passwordHash(body.newPassword);
//         const result = await updateUser({ body: { password: hash }, auth });

    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // // /**
    // //  * Subscribe to delete one user
    // //  */
    // // ACTIONS.on(users_delete, async({ params }) => {
    // //     try {
    // //         const result = await deleteUser({ uid: params.id });

    // //         return result;
    // //     } catch (error) {
    // //         const message = error.message;

    // //         return Promise.reject({ code: 400, message, ...error });
    // //     }
    // // });

    // // /**
    // //  * Subscribe to get all users by filters
    // //  */
    // // ACTIONS.on(users_all, async(/* { query } */) => {
    // //     try {
    // //         // controller call's
    // //         const result = await getAllUsers(/* { payload: query } */);

    // //         return result;
    // //     } catch (error) {
    // //         const message = error.message;

    // //         return Promise.reject({ code: 400, message, ...error });
    // //     }
    // // });

    // /**
    //  * Get validation schema
    // */
    ACTIONS.on('posts.validation.schema.get', () => {
        return Promise.resolve(postEntity.validation);
    });

    /**
     * Custom query
    */
    ACTIONS.on('posts.custom.query', (payload) => {
        return Promise.resolve(userService.custom(payload));
    });

   /**
    * Post init application action
   */
    ACTIONS.on('postinit.users', () => {        
        return Promise.resolve('success');
    });
};
