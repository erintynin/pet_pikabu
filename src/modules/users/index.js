const { routes } = require('./config.json');
// Import middlewares
const {
    userExistCheckMiddleware,
    userPermissionMiddleware,
} = require('./middlewares/index');
// Import controllers
const { _userController } = require('./controllers/index');
// Import services
const {
    _userService, _jwtService, _fsService,
} = require('./services/index');
// Import entity
const { 
    userEntity,
    // comentEntity,
    // postEntity,
    saveEntity,
    subscriberEntity,
    subscriptionEntity,
} = require('./entity/index');

const userSubscriptions = require('./subscriptions');

/**
 * Configurate user module
*/
module.exports = async({ ACTIONS, ROUTER, ERROR, CONFIG, Model, utils }) => {
    /**
    * Add config routes to global ROUTER
    */
    ROUTER.set('routes', routes);
    /**
    * Create user MODEL from schema
    */
    const userModel = new Model('users', userEntity);
    // const postModel = new Model('posts', postEntity);
    // const comentModel = new Model('coments', comentEntity);
    const saveModel = new Model('saves', saveEntity);
    const subscriberModel = new Model('subscribers', subscriberEntity);
    const subscriptionModel = new Model('subscriptions', subscriptionEntity);

    
    /**
    * Pin ACTIONS to services
    */
    const userService = _userService({ ACTIONS, userModel });
    const jwtService = _jwtService({ ACTIONS });
    const fsService = _fsService({ ACTIONS });
    /**
    * Pin ACTIONS to controllers
    */
    const userController = _userController({
        ACTIONS, CONFIG, userService, jwtService, fsService,
    });
    /**
    * Transform route names to ACTIONS commands
    */
    const userCommands = utils.convertKeysToDots(routes);
    /**
    * Transform route names to ACTIONS commands
    */
    userSubscriptions({
        ACTIONS, userController, userCommands, userEntity, userService,
    });
    /**
    * Add user middlewares to global ROUTER
    */   
    ROUTER.set('middlewares', {
        userExistCheckMiddleware: userExistCheckMiddleware({
            ACTIONS, ERROR, userService,
        }),
        userPermissionMiddleware: userPermissionMiddleware({
            ACTIONS, ROUTER, CONFIG, ERROR,
        }),
    }, 'routes');
};
