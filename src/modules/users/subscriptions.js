module.exports = ({
    ACTIONS, userController, userCommands, userEntity, userService,
}) => {
    /**
     * Extract user commands
     */
    
    const {
        // eslint-disable-next-line camelcase
        users_register, users_auth, users_facebook, users_google, users_update, 
        // eslint-disable-next-line camelcase
        users_upemail, users_uppassword, users_get,
        // 
        // users_delete,
        // users_all,
    } = userCommands;
    /**
     * Extract user controller methods
     */
    const {
        createUser,
        getSelf,
        parseProfile,
        comparePassword,
        getUser,
        updateUser,
        passwordHash,
        // deleteUser,
        // getAllUsers,
    } = userController;    

    /**
     * Subscribe to register user
     */
    ACTIONS.on(users_register, async({ body, auth }) => {
        try {
            // checkers
            if (auth) throw new Error('User already exist!');
            
            // controller call's
            const result = await createUser({ body });
            // handle result

            return result.publicKeys();
        } catch (error) {
            const message = error.message || error;

            return Promise.reject({ code: 400, message, ...error });
        }
    });

    /**
     * Subscribe to auth user
     */
    ACTIONS.on(users_auth, async({ auth }) => {
        try {
            // checkers
            if (!auth) throw new Error('No such user here!');
            // controller call's
            const result = await getSelf({ auth });

            return result.publicKeys();
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    /**
     * Subscribe to user auth facebook 
     */
    ACTIONS.on(users_facebook, async({ auth, body }) => {
        try {
            // checkers
            if (!auth && !body) throw new Error('No such user here!');
            if (!auth) auth = await createUser(await parseProfile(body));
            // controller call's
            const result = await getSelf({ auth });

            return result.publicKeys();
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    /**
     * Subscribe to user auth facebook 
     */
    ACTIONS.on(users_google, async({ auth, body }) => {
        try {
            // checkers
            if (!auth && !body) throw new Error('No such user here!');
            if (!auth) auth = await createUser(await parseProfile(body));
            // controller call's
            const result = await getSelf({ auth });

            return result.publicKeys();
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });

    // /**
    //  * Subscribe to get one user
    //  */
    ACTIONS.on(users_get, async({ params }) => {
        try {
            // controller call's
            const result = await getUser({ id: params.id });

            return result.publicKeys();
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });

    /**
     * Subscribe to update user
     */
    ACTIONS.on(users_update, async({ body, auth }) => {
        try {
            if (!auth) throw new Error('No such user here!');

            const result = await updateUser({ body, auth });

            return result;
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    /**
     * Subscribe to update email user
     */
    ACTIONS.on(users_upemail, async({ body, auth }) => {
        try {
            if (!auth) throw new Error('No such user here!');
            
            const compare = await comparePassword({ auth, body });
            if (!compare) throw new Error(compare.error);

            return await updateUser({ body: { email: body.email }, auth });
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });

    ACTIONS.on(users_uppassword, async({ body, auth }) => {
        try {
            if (!auth) throw new Error('No such user here!');
            
            const compare = await comparePassword({ auth, body });
            if (!compare) throw new Error(compare.error);
            const hash = await passwordHash(body.newPassword);
            const result = await updateUser({ body: { password: hash }, auth });

            return result;
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    // /**
    //  * Subscribe to delete one user
    //  */
    // ACTIONS.on(users_delete, async({ params }) => {
    //     try {
    //         const result = await deleteUser({ uid: params.id });

    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // /**
    //  * Subscribe to get all users by filters
    //  */
    // ACTIONS.on(users_all, async(/* { query } */) => {
    //     try {
    //         // controller call's
    //         const result = await getAllUsers(/* { payload: query } */);

    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // /**
    //  * Get validation schema
    // */
    ACTIONS.on('users.validation.schema.get', () => {
        return Promise.resolve(userEntity.validation);
    });

    /**
     * Custom query
    */
    ACTIONS.on('users.custom.query', (payload) => {
        return Promise.resolve(userService.custom(payload));
    });

   /**
    * Post init application action
   */
    ACTIONS.on('postinit.users', () => {        
        return Promise.resolve('success');
    });
};
