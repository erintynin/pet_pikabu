/* eslint-disable max-len */
module.exports = (schema) => {
    // statics
    schema.methods.publicKeys = function() {
        return {
            id: this.id,
            nickname: this.nickname,
            email: this.email,
            accessToken: this.accessToken,
        };
    };

    return schema;
};
