/* eslint-disable max-len */
module.exports = {
    email: {
        type: 'string',
        default: '',
    },
    // role: {
    //     type: 'string',
    //     default: (process.env.NODE_ENV == 'production') ? 'user' : 'admin',
    // },
    password: {
        type: 'string',
        default: '',
    },
    nickname: {
        type: 'string',
        default: 'John',
    },
    photo: {
        type: 'string',
        default: '',
    },
    yourself: {
        type: 'string',
        default: '',
    },
    gender: {
        type: 'string',
        default: '',
    },
    config: {
        type: 'string',
        default: '',
    },
};
