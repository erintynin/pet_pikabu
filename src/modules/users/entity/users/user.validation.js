/* eslint-disable max-len */
module.exports = {
    id: {
        number: true,
        min: 1,
        error: () => `Invalid id, must be number`,
    },
    nickname: {
        string: true,
        regex: new RegExp(/^[а-яА-ЯёЁa-zA-Z0-9]{3,30}/g),
        error: () => `Invalid nickname, 3-30 symbols required, Upper/lower case, nums, -, _`,
    },
    email: {
        string: true,
        regex: new RegExp(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/),
        min: 6,
        max: 40,
        error: () => `Invalid email, must be an email format with 6-40 symbols`,
    },
    password: {
        string: true,
        regex: new RegExp(/^(?=.*\d)[A-Za-z\d@$!%*#?&^!()]{6,30}$/g),
        error: () => `Invalid password, 6-30 symbols required, at least one numeral`,
    },
    newPassword: {
        string: true,
        regex: new RegExp(/^(?=.*\d)[A-Za-z\d@$!%*#?&^!()]{6,30}$/g),
        error: () => `Invalid password, 6-30 symbols required, at least one numeral`,
    },
    yourself: {
        string: true,
        max: 150,
        error: () => `Invalid yourself format, max 300 symbols required`,
    },
    photo: {
        string: true,
        min: 15,
        max: 300,
        error: () => `Invalid photo format, 15-300 symbols required`,
    },
    gender: {
        string: true,
        valid: ['women', 'men'],
        error: () => `Invalid gender format, must be women or men`,
    },
};
