/**
  ******************************************
  * Express/connect/restify implementation *
  ******************************************
  * @param  {Events} ACTIONS
  * @param  {function} req - request
  * @param  {function} res - response
  * @param  {function} next - next
  * @return {function} - next
  */
module.exports = ({ ACTIONS, ERROR, userService }) => async(req, res, next) => {
    try {
        const user = await userService.readRecord({ email: req.body.email });

        req.auth = user;

        next();
    } catch (error) {
        const { status, result } = ERROR.create(error);
        res.status(status).send(result);
    }
};
