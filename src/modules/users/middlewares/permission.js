/**
 ******************************************
 * Express/connect/restify implementation *
 ******************************************
 * @param  {Events} ACTIONS
 * @param  {Router} ROUTER
 * @param  {function} req - request
 * @param  {function} res - response
 * @param  {function} next - next
 * @return {function} - next
 */
module.exports = ({ ROUTER, ERROR }) => async(req, res, next) => {
    try {
        if (!req.auth) return res.send({ error: 403, message: 'Auth error!' });

        const allRoutes = ROUTER.get('routes');
        const routePath = req.route.path;
        const currentRoute = Object
            .values(allRoutes).find((r) => r.path === routePath.substring(5));

        const errorMessage = `Access danied for role '${
            req.auth.role
            }'! Needed role '${currentRoute.access}'`;

        if (currentRoute.access.indexOf(req.auth.role) == -1) {
            return res.send({ error: 403, message: errorMessage });
        }

        next();
    } catch (error) {
        const { status, result } = ERROR.create(error);

        res.status(status).send(result);
    }
};
