module.exports = ({ ACTIONS }) => ({
    /**
     * Abstract method to get JWT TOKEN
     */
    getNewJWT: async({ id, expire }) =>
        ACTIONS.send('passport.create.jwt', { id, expire }),
    refreshJWT: () => {

    },
    parseJWT: () => {},
});
