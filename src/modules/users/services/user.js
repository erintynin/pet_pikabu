// const nanoid = require('nanoid');
const crypto = require('crypto');

module.exports = ({ ACTIONS, userModel }) => ({
    newUserData: ({ password, secretkey, accessExpire }) => {
        // const uid = nanoid();
        const hash = password ? crypto
            .createHmac('sha256', secretkey)
            .update(password).digest('hex'): '';
        const expire = Date.now() + (accessExpire || 155520000);

        return { expire, hash };
    },
    createRecord: (payload) => {
        const userOptions = { model: userModel.model, payload };

        return ACTIONS.send('database.create', userOptions);
    },
    readRecord: (payload) => {
        const userOptions = { model: userModel.model, payload };

        return ACTIONS.send('database.read', userOptions);
    },
    comparePassword: ({ bodyPassword, authPassword, secretkey }) => {
        return crypto.createHmac('sha256', secretkey)
        .update(bodyPassword).digest('hex') === authPassword ? true : false;
    },

    readRecordById: (id) => {
        const userOptions = { model: userModel.model, payload: { id } };

        return ACTIONS.send('database.read', userOptions);
    },

    // deleteRecordById: (uid) => {
    //     const userOptions = { model: userModel.model, payload: { uid } };

    //     return ACTIONS.send('database.delete', userOptions);
    // },
    // getManyRecords: (payload = {}) => {
    //     const userOptions = { model: userModel.model, payload };

    //     return ACTIONS.send('database.readAll', userOptions);
    // },

    custom: ({ type = 'read', payload = {}, 
        data = {}, op = null, attributes = null }) => {
        const userOptions = { model: userModel.model, payload, op, attributes };

        if (type === 'update') userOptions.data = data;

        return ACTIONS.send(`database.${type}`, userOptions);
    },
});
