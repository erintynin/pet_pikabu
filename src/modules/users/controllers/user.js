const TYPE = 'controller';

module.exports = ({ CONFIG, userService, jwtService, fsService }) => ({
    /**
     * CREATE USER
     */
    createUser: async({
        body: { email = '', nickname = '', password = '' },
    }) => {
        try {
            const { expire, hash } = userService
                .newUserData({ password, ...CONFIG });
            const user = await userService
                .createRecord({ email, nickname, password: hash });
            const accessToken = await jwtService
                .getNewJWT({ id: user.id, expire });
            
        // fsService
        //     .createDirectory(CONFIG.pathToBoxes, `${username}.webboxes.io`);

            user.accessToken = accessToken;

            return user;
        } catch (error) {
            return Promise.reject(error);
        }
    },

    parseProfile: async(body = {}) => {
        const { email, id, sub } = body;
        return {body: { email, nickname: id || sub }};    
    },

    comparePassword: async({ body, auth }) => {
        const result = await userService.comparePassword({
            bodyPassword: body.password, authPassword: auth.password, ...CONFIG,
        });
        return result ? true : { error: '425'};
    },

    passwordHash: async(password) => {
        const { hash } = await userService
            .newUserData({ password, ...CONFIG });
        return hash;
    },

    /**
    * GET SELF INFO
    */
    getSelf: async({ auth }) => {
        try {
            const accessToken = await jwtService.getNewJWT({
                id: auth.id,
                expire: Date.now() + (CONFIG.accessExpire || 155520000),
            });

            auth.accessToken = accessToken;

            return auth;
        } catch (error) {
            return Promise.reject('400');
        }
    },
    /**
    * GET USER INFO
    */
    getUser: async({ id }) => {
        try {
            const user = await userService.readRecordById(id);

            return user;
        } catch (error) {
            return Promise.reject('400');
        }
    },
    /**
    * UPDATE USER INFO
    */
    updateUser: async({ body, auth }) => {
        try {
            for (key in body) {
                if (auth[key] !== body[key]) auth[key] = body[key];
            }

            return auth.save();
        } catch (error) {
            return Promise.reject('400');
        }
    },
    // /**
    // * DELETE USER
    // */
    // deleteUser: async({ uid }) => {
    //     try {
    //         const user = await userService.deleteRecordById(uid);

    //         if (!user) throw new Error('Can\'t delete user!');

    //         return { delete: true };
    //     } catch (error) {
    //         return Promise.reject('400');
    //     }
    // },
    // /**
    // * GET ALL USERS
    // */
    // getAllUsers: async() => {
    //     try {
    //         const users = await userService.getManyRecords();

    //         return users;
    //     } catch (error) {
    //         return Promise.reject((error.type = TYPE, error));
    //     }
    // },
});

