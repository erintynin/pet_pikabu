
/**
 * Configurate user module
*/
module.exports = async({ ACTIONS, ROUTER, ERROR, CONFIG, Model, utils }) => {
    ACTIONS.on('handlemodels', () => {  
        Model.getAllModelReady().then(
            async() => {
                const models = Model.getAllModelList();
                models['users'].hasMany(models['posts']);
                models['comments'].hasMany(models['comments']);
                models['posts'].belongsTo(models['users']);
                
                models['posts'].hasMany(models['comments']);
                models['comments'].belongsTo( models['posts']);
                
                models['users'].hasMany(models['comments']);
                models['comments'].belongsTo(models['users']);

                models['users'].belongsToMany(models['posts'], 
                    { through: models['saves'] });
                    
                models['users'].hasMany(models['subscriptions']);
                models['subscribers'].hasMany(models['users']);

                models['posts'].hasOne(models['postratings']);
                models['postratings'].belongsTo(models['posts']);
                // const res = models['users'].getAssociations(models['posts']);

                ACTIONS.send('database.sync.forse');
            }
        );      
        return Promise.resolve('success');
    });
};
