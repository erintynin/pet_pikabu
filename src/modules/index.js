/**
 * The root path of modules.
 * Export your modules here to connect it to main core.
 */

const _usersModules = require('./users/index');
const _generalsModules = require('./generals/index');
const _postsModules = require('./posts/index');
const _comentsModules = require('./coments/index');

module.exports = () => [
    _usersModules, _generalsModules, _postsModules, _comentsModules,
];
