const fs = require('fs');
const path = require('path');

module.exports = () => ({
    createDirectory: (rootDir = '/', dir = '') => {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(path.join(rootDir, dir));

            return true;
        }
    },
});
