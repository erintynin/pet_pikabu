module.exports = ({
    ACTIONS, commentController, commentCommands, 
    commentEntity, commentService, Model,
}) => {
    /**
     * Extract user commands
     */
    
    const {
        // eslint-disable-next-line camelcase
        comments_post, comments_comment, comments_getpost, comments_new, 
        // eslint-disable-next-line camelcase
        users_upemail, users_uppassword, users_get,
        // 
        // users_delete,
        // users_all,
    } = commentCommands;
    /**
     * Extract user controller methods
     */
    const {
        addDot,
        getHot,
        getBest,
        getNew,
        // parseProfile,
        // comparePassword,
        // getUser,
        // updateUser,
        // passwordHash,
        // deleteUser,
        // getAllUsers,
    } = commentController;    

    /**
     * Subscribe to comments create
     */
    ACTIONS.on(comments_post, async({ body, auth }) => {
        try {
            const { postId } = body;
            if (!auth) throw new Error('User already exist!');
            const comments = await auth.createComment({ 
                body: body.body, postId, 
            });
            comments.tree = '' + comments.id;
            return comments.save();
        } catch (error) {
            const message = error.message || error;
            return Promise.reject({ code: 400, message, ...error });
        }
    });

    /**
     * Subscribe to get hot comments
     */
    ACTIONS.on(comments_comment, async({ body, auth }) => {
        try {
            const { tree, commentId } = body;
            let parentTree = tree;
            if (!auth) throw new Error('User already exist!');
            if (!(tree[tree.length - 1] === '.')) {
                parentTree = await addDot({ id: commentId, tree });
            }
            const comments = await auth.createComment({ 
                body: body.body, tree: parentTree + '1', commentId: commentId, 
            });
            comments.save();
            return comments;
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });

    ACTIONS.on(comments_getpost, async({ params, auth }) => {
        try {
            if (!auth) throw new Error('User already exist!');
            const { postId } = params;
            return commentService.getManyRecords({
                payload: { postId },
                model: 'comments',
            });
        } catch (error) {
            const message = error.message;

            return Promise.reject({ code: 400, message, ...error });
        }
    });
    /**
     * Subscribe to get best comments
     */
    // ACTIONS.on(comments_best, async({ auth }) => {
    //     try {
    //         // checkers
    //         if (!auth) throw new Error('No such user here!');
    //         // controller call's
    //         const result = await getBest();
            
    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // ACTIONS.on(comments_new, async({ auth }) => {
    //     try {
    //         // checkers
    //         if (!auth) throw new Error('No such user here!');
    //         // controller call's
    //         const result = await getNew();
            
    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // /**
    //  * Subscribe to user auth facebook 
    //  */
    // ACTIONS.on(users_facebook, async({ auth, body }) => {
    //     try {
    //         // checkers
    //         if (!auth && !body) throw new Error('No such user here!');
    //         if (!auth) auth = await createUser(await parseProfile(body));
    //         // controller call's
    //         const result = await getSelf({ auth });

    //         return result.publicKeys();
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // /**
    //  * Subscribe to user auth facebook 
    //  */
    // ACTIONS.on(users_google, async({ auth, body }) => {
    //     try {
    //         // checkers
    //         if (!auth && !body) throw new Error('No such user here!');
    //         if (!auth) auth = await createUser(await parseProfile(body));
    //         // controller call's
    //         const result = await getSelf({ auth });

    //         return result.publicKeys();
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // // /**
    // //  * Subscribe to get one user
    // //  */
    // ACTIONS.on(users_get, async({ params }) => {
    //     try {
    //         // controller call's
    //         const result = await getUser({ id: params.id });

    //         return result.publicKeys();
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // /**
    //  * Subscribe to update user
    //  */
    // ACTIONS.on(users_update, async({ body, auth }) => {
    //     try {
    //         if (!auth) throw new Error('No such user here!');

    //         const result = await updateUser({ body, auth });

    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // /**
    //  * Subscribe to update email user
    //  */
    // ACTIONS.on(users_upemail, async({ body, auth }) => {
    //     try {
    //         if (!auth) throw new Error('No such user here!');
            
    //         const compare = await comparePassword({ auth, body });
    //         if (!compare) throw new Error(compare.error);

    //         return await updateUser({ body: { email: body.email }, auth });
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });

    // ACTIONS.on(users_uppassword, async({ body, auth }) => {
    //     try {
    //         if (!auth) throw new Error('No such user here!');
            
    //         const compare = await comparePassword({ auth, body });
    //         if (!compare) throw new Error(compare.error);
    //         const hash = await passwordHash(body.newPassword);
    //    const result = await updateUser({ body: { password: hash }, auth });

    //         return result;
    //     } catch (error) {
    //         const message = error.message;

    //         return Promise.reject({ code: 400, message, ...error });
    //     }
    // });
    // // /**
    // //  * Subscribe to delete one user
    // //  */
    // // ACTIONS.on(users_delete, async({ params }) => {
    // //     try {
    // //         const result = await deleteUser({ uid: params.id });

    // //         return result;
    // //     } catch (error) {
    // //         const message = error.message;

    // //         return Promise.reject({ code: 400, message, ...error });
    // //     }
    // // });

    // // /**
    // //  * Subscribe to get all users by filters
    // //  */
    // // ACTIONS.on(users_all, async(/* { query } */) => {
    // //     try {
    // //         // controller call's
    // //         const result = await getAllUsers(/* { payload: query } */);

    // //         return result;
    // //     } catch (error) {
    // //         const message = error.message;

    // //         return Promise.reject({ code: 400, message, ...error });
    // //     }
    // // });

    // /**
    //  * Get validation schema
    // */
    ACTIONS.on('comments.validation.schema.get', () => {
        return Promise.resolve(commentEntity.validation);
    });

    /**
     * Custom query
    */
    ACTIONS.on('comments.custom.query', (payload) => {
        return Promise.resolve(userService.custom(payload));
    });

   /**
    * comment init application action
   */
    ACTIONS.on('commentinit.users', () => {        
        return Promise.resolve('success');
    });
};
