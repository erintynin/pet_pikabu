const { routes } = require('./config.json');
// Import middlewares
const {

} = require('./middlewares/index');
// Import controllers
const { _commentController } = require('./controllers/index');
// Import services
const {
    _commentService, _fsService,
} = require('./services/index');
// Import entity
const { 
    commentEntity,
} = require('./entity/index');

const commentSubscriptions = require('./subscriptions');

/**
 * Configurate user module
*/
module.exports = async({ ACTIONS, ROUTER, ERROR, CONFIG, Model, utils }) => {
    /**
    * Add config routes to global ROUTER
    */
    ROUTER.set('routes', routes);
    /**
    * Create user MODEL from schema
    */
    const commentModel = new Model('comments', commentEntity);
    const models = Model.getAllModelList();
    /**
    * Pin ACTIONS to services
    */
    const commentService = _commentService({ ACTIONS, commentModel, models });
    const fsService = _fsService({ ACTIONS });
    /**
    * Pin ACTIONS to controllers
    */
    const commentController = _commentController({
        ACTIONS, CONFIG, commentService, fsService,
    });
    /**
    * Transform route names to ACTIONS commands
    */
    const commentCommands = utils.convertKeysToDots(routes);
    /**
    * Transform route names to ACTIONS commands
    */
    commentSubscriptions({
        ACTIONS, commentController, commentCommands,
            commentEntity, commentService, Model,
    });
    /**
    * Add user middlewares to global ROUTER
    */   
    // ROUTER.set('middlewares', {
    //     commentExistCheckMiddleware: commentExistCheckMiddleware({
    //         ACTIONS, ERROR, commentService,
    //     }),
    //     commentPermissionMiddleware: commentPermissionMiddleware({
    //         ACTIONS, ROUTER, CONFIG, ERROR,
    //     }),
    // }, 'routes');
};
