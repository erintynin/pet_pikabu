/* eslint-disable max-len */
module.exports = {
    body: {
        string: true,
        max: 400,
        error: () => `Invalid body, max length 400.`,
    },
    postId: {
        number: true,
        min: 1,
        error: () => `Invalid postId, must be type number.`,
    },
    commentId: {
        number: true,
        min: 1,
        error: () => `Invalid commentId, must be type number.`,
    },
    tree: {
        string: true,
        min: 1,
        error: () => `Invalid commentId, must be type string.`,
    },
};
