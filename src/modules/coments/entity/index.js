const commentSchema = require('./comments/comment.schema');
// const postSchema = require('./posts/post.schema');
// const postRatingsSchema = require('./postRatings/postRatings.schema');
// const subscriberSchema = require('./subscribers/subscriber.schema');
// const subscriptionSchema = require('./subscriptions/subscription.schema');
// const userSchema = require('./users/user.schema');


const commentStatics = require('./comments/comment.statics');
// const postStatics = require('./posts/post.statics');
// const postRatingsStatics = require('./postRatings/postRatings.statics');
// const subscriberStatics = require('./subscribers/subscriber.statics');
// const subscriptionStatics = require('./subscriptions/subscription.statics');
// const userStatics = require('./users/user.statics');

const commentValidation = require('./comments/comment.validation');
// const postValidation = require('./posts/post.validation');
// const postRatingsValidation = 
// require('./postRatings/postRatings.validation');
// const subscriberValidation = require('./subscribers/subscriber.validation');
// const subscriptionValid = require('./subscriptions/subscription.validation');
// const userValidation = require('./users/user.validation');

// const saveOptions = require('./saves/save.options');

module.exports = {
    // postRatingsEntity: {
    //     schema: postRatingsSchema,
    //     statics: postRatingsStatics,
    //     validation: postRatingsValidation,
    //     relationships: () => {},
    // },
    commentEntity: {
        schema: commentSchema,
        statics: commentStatics,
        validation: commentValidation,
        relationships: () => {},
    },
    // postEntity: {
    //     schema: postSchema,
    //     statics: postStatics,
    //     validation: postValidation,
    //     relationships: () => {},
    // },
    // saveEntity: {
    //     schema: saveSchema,
    //     statics: saveStatics,
    //     validation: saveValidation,
    //     options: saveOptions,
    //     relationships: () => {},
    // },
    // subscriberEntity: {
    //     schema: subscriberSchema,
    //     statics: subscriberStatics,
    //     validation: subscriberValidation,
    //     relationships: () => {},
    // },
    // subscriptionEntity: {
    //     schema: subscriptionSchema,
    //     statics: subscriptionStatics,
    //     validation: subscriptionValid,
    //     relationships: () => {},
    // },
};
