const TYPE = 'controller';

module.exports = ({ CONFIG, commentService, fsService }) => ({
    /**
     * CREATE USER
     */
    addDot: async({ id, tree }) => {
        try {
            const attributes = ['tree', 'id'];
            const payload = { id };
            const comment = await commentService
                .readRecord({ payload, attributes });
            if (~comment.tree.indexOf(tree + '.')) return tree + '.';
            comment.tree += '.';
            comment.save();
            return comment.tree;
        } catch (error) {
            return Promise.reject(error);
        }
    },

    getHot: async() => {
        const createdAt = Date.now() - 432000000;
        let payload = {};
        let op = [];
        const order = [['rating', 'DESC']];
        payload.createdAt = createdAt, op.push('gte');
        const modelsInclude = { comments: true };
        const res = commentService.getManyRecords({
            payload, op, order, model: 'commentratings', modelsInclude });
        return res;    
    },

    getBest: async() => {
        const createdAt = Date.now() - 2678400000;
        let payload = {};
        let op = [];
        const order = [['like', 'DESC']];
        payload.createdAt = createdAt, op.push('gte');
        const res = commentService.getManyRecords({
            payload, op, order, model: 'comments' });
        return res;    
    },

    getNew: async() => {
        let op = [];
        const order = [['createdAt', 'DESC']];
        return commentService.getManyRecords({ 
            op, order, model: 'comments', 
        });    
    },

    comparePassword: async({ body, auth }) => {
        const result = await userService.comparePassword({
            bodyPassword: body.password, authPassword: auth.password, ...CONFIG,
        });
        return result ? true : { error: '425'};
    },

    passwordHash: async(password) => {
        const { hash } = await userService
            .newUserData({ password, ...CONFIG });
        return hash;
    },

    /**
    * GET SELF INFO
    */
    getSelf: async({ auth }) => {
        try {
            const accessToken = await jwtService.getNewJWT({
                id: auth.id,
                expire: Date.now() + (CONFIG.accessExpire || 155520000),
            });

            auth.accessToken = accessToken;

            return auth;
        } catch (error) {
            return Promise.reject('400');
        }
    },
    /**
    * GET USER INFO
    */
    getUser: async({ id }) => {
        try {
            const user = await userService.readRecordById(id);

            return user;
        } catch (error) {
            return Promise.reject('400');
        }
    },
    /**
    * UPDATE USER INFO
    */
    updateUser: async({ body, auth }) => {
        try {
            for (key in body) {
                if (auth[key] !== body[key]) auth[key] = body[key];
            }

            return auth.save();
        } catch (error) {
            return Promise.reject('400');
        }
    },
    // /**
    // * DELETE USER
    // */
    // deleteUser: async({ uid }) => {
    //     try {
    //         const user = await userService.deleteRecordById(uid);

    //         if (!user) throw new Error('Can\'t delete user!');

    //         return { delete: true };
    //     } catch (error) {
    //         return Promise.reject('400');
    //     }
    // },
    // /**
    // * GET ALL USERS
    // */
    // getAllUsers: async() => {
    //     try {
    //         const users = await userService.getManyRecords();

    //         return users;
    //     } catch (error) {
    //         return Promise.reject((error.type = TYPE, error));
    //     }
    // },
});

